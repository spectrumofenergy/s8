//
//  ViewController.swift
//  Esercitazione 3
//
//  Created by Wakala on 23/01/2019.
//  Copyright © 2019 Consoft. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textFields: UITextField!
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFields.delegate = self
        logoOnBar()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        label.text = textField.text
        return true
    }
    
    @IBAction func pippo(forUnwind segue: UIStoryboardSegue) {
    }
    
    private func logoOnBar() {
        let image = UIImage(named: "Logo")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "view1Segue" {
            let vc1 = segue.destination as! ViewController1
            vc1.text = textFields.text
        }
    }
}

